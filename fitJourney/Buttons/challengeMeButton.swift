//
//  challengeMeButton.swift
//  fitJourney
//
//  Created by Victoria Alvizurez on 2/2/18.
//  Copyright © 2018 Victoria Alvizurez. All rights reserved.
//

import UIKit

class challengeMeButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setImage( UIImage(named: "fist.png"), for: UIControlState.normal)
         self.imageView?.contentMode = UIViewContentMode.scaleAspectFit;
        //TODO: Code for our button
    }

}
