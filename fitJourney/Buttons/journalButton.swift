//
//  journalButton.swift
//  fitJourney
//
//  Created by Victoria Alvizurez on 2/2/18.
//  Copyright © 2018 Victoria Alvizurez. All rights reserved.
//

import UIKit

class journalButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setImage( UIImage(named: "journal.png"), for: UIControlState.normal)
        
        //TODO: Code for our button
    }
}
