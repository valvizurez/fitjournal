//
//  SignUpViewController.swift
//  fitJourney
//
//  Created by Victoria Alvizurez on 2/2/18.
//  Copyright © 2018 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class SignUpViewController: UIViewController {
    var ref: DatabaseReference!
    
    @IBOutlet weak var userFirstName: UITextField!
    @IBOutlet weak var userLastName: UITextField!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    @IBOutlet weak var userBirthDate: UITextField!
    @IBOutlet weak var userName: UITextField!
    
    
    
    let datePicker = UIDatePicker()
    
    var handle: AuthStateDidChangeListenerHandle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userBirthDate.delegate = self as? UITextFieldDelegate
        showDatePicker()
        
        let ref = Database.database().reference()
        
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
      
        }
        let email = userEmail.text
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // [START remove_auth_listener]
        Auth.auth().removeStateDidChangeListener(handle!)
        // [END remove_auth_listener]
    }
    
    @IBAction func signUP(_ sender: Any) {
        Auth.auth().createUser(withEmail: userEmail.text!, password: userPassword.text!) { (user, error) in
                if let error = error {
                    print(error)
                    return
               }
                else{
                let userData = ["firstName": self.userFirstName.text, "lastName": self.userLastName.text, "birthDate": self.userBirthDate.text, "userName": self.userName.text]
                let ref = Database.database().reference()
                ref.child("users").child(user!.uid).child("info").setValue(userData)
                print("\(user!.email!) created")
            DispatchQueue.global().async {
                DispatchQueue.main.async {
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "userJournal") as! UserJournalViewController
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
            }
        }
    }

    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.bordered, target: self,  action: #selector(SignUpViewController.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.bordered, target: self, action:#selector(SignUpViewController.cancelDatePicker))
        
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        // add toolbar to textField
        userBirthDate.inputAccessoryView = toolbar
        // add datepicker to textField
        userBirthDate.inputView = datePicker
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        userFirstName.resignFirstResponder()
        userLastName.resignFirstResponder()
        userEmail.resignFirstResponder()
        userPassword.resignFirstResponder()
        return true
        
    }
    
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        userBirthDate.text = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }

}
