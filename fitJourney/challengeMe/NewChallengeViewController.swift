//
//  NewChallengeViewController.swift
//  fitJourney
//
//  Created by Victoria Alvizurez on 3/23/18.
//  Copyright © 2018 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage

class NewChallengeViewController: UIViewController {

    @IBOutlet weak var StartDate: UITextField!
    @IBOutlet weak var EndDate: UITextField!
    @IBOutlet weak var ChallengeMeDescription: UITextView!

    @IBOutlet weak var ChallengeMeTitle: UITextField!
    let timePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showTimePicker()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func postNewChallenge(_ sender: Any) {
        let userID = Auth.auth().currentUser!.uid
        let challengeMePostData = ["ChallengeMeTitle": self.ChallengeMeTitle.text, "challengeMeDescription": self.ChallengeMeDescription.text, "StartDate": self.StartDate.text, "EndDate": self.EndDate.text] as [String : Any]
        
        let ref = Database.database().reference()
        ref.child("challengeMePosts").child(userID).child("posts").childByAutoId().setValue(challengeMePostData)
        
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "challengeMe") as! ChallengeMeViewController
                self.present(vc, animated: true, completion: nil)
            }
        }
        
    }
    func showTimePicker(){
        //Formate Date
        timePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.bordered, target: self,  action: #selector(NewChallengeViewController.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.bordered, target: self, action:#selector(NewChallengeViewController.cancelDatePicker))
        
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        // add toolbar to textField
        StartDate.inputAccessoryView = toolbar
        // add datepicker to textField
        StartDate.inputView = timePicker
        
        EndDate.inputAccessoryView = toolbar
        // add datepicker to textField
        EndDate.inputView = timePicker
    }
    
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/YYYY"
        if (StartDate.isEditing){
            StartDate.text = formatter.string(from: timePicker.date)
        }
        else{
            EndDate.text = formatter.string(from: timePicker.date)
        }
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
}
