//
//  ChallengeMeBoardCellsCollectionViewCell.swift
//  fitJourney
//
//  Created by Victoria Alvizurez on 3/18/18.
//  Copyright © 2018 Victoria Alvizurez. All rights reserved.
//

import UIKit

class ChallengeMeBoardCellsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var ChallengeMeDateRange: UILabel!
    @IBOutlet weak var ChallengeMeTitle: UILabel!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
