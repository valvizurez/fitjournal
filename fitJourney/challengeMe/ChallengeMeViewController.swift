//
//  ChallengeMeViewController.swift
//  fitJourney
//
//  Created by Victoria Alvizurez on 2/2/18.
//  Copyright © 2018 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth

class ChallengeMeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var ChallengeMeCollectionView: UICollectionView!
    
    var ref = Database.database().reference()
    var loggedInUser:AnyObject?
    var loggedInUserData:NSDictionary?
    var challengeMeBoards = [NSDictionary]()
    
    func collectionView(_ ChallengeMeCollectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        // return (flickrResults as AnyObject).count
        return self.challengeMeBoards.count
    }
    
    func collectionView(_ ChallengeMeCollectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = ChallengeMeCollectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                                 for: indexPath) as! ChallengeMeBoardCellsCollectionViewCell
        cell.ChallengeMeTitle.text = challengeMeBoards[(self.challengeMeBoards.count-1) - (indexPath.row)]["ChallengeMeTitle"] as! String
        var startDate = challengeMeBoards[(self.challengeMeBoards.count-1) - (indexPath.row)]["StartDate"] as! String
        var endDate = challengeMeBoards[(self.challengeMeBoards.count-1) - (indexPath.row)]["EndDate"] as! String
        cell.ChallengeMeDateRange?.text =  endDate
        cell.ChallengeMeDateRange?.sizeToFit()
        cell.backgroundView?.backgroundColor = UIColor.white
        
        
       // cell.ChallengeMeTitle?.text = "Test"
        
        return cell
    }
    
 
    
    func collectionView(_ ChallengeMeCollectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width/2 - 40, height: self.view.frame.size.width/2 - 40)
    }

    func numberOfSections(in ChallengeMeCollectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        challengeMeBoards = [NSDictionary]()
         self.ChallengeMeCollectionView.register(UINib(nibName: "ChallengeMeBoardCellsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
         self.loggedInUser = Auth.auth().currentUser
        
        getAllChallengeMe()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    func getAllChallengeMe(){
        let userID = Auth.auth().currentUser!.uid
        let ref = Database.database().reference()
        //ref.child("challengeMePosts").child(userID).child("posts").childByAutoId().setValue(challengeMePostData)
        
        
        ref.child("challengeMePosts").child(userID).child("posts").observe(.childAdded, with: { (snapshot:DataSnapshot) in
            self.challengeMeBoards.append(snapshot.value as! NSDictionary)
            
            self.ChallengeMeCollectionView.insertItems(at: [IndexPath(row:0,section:0)])
            //self.ChallengeMeCollectionView.insertRows(at: [IndexPath(row:0,section:0)], with: UITableViewRowAnimation.automatic)
          //  self.loadingAiv.stopAnimating()
        })
        {(error) in
            
            print(error.localizedDescription)
        }
    //    self.userTimeLine.rowHeight = UITableViewAutomaticDimension
      //  self.userTimeLine.estimatedRowHeight = 140
        
          // self.ChallengeMeCollectionView.reloadData()
    }
    

}
