//
//  UserJournalViewController.swift
//  fitJourney
//
//  Created by Victoria Alvizurez on 2/2/18.
//  Copyright © 2018 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth

class UserJournalViewController: UIViewController,UITableViewDataSource, UITableViewDelegate  {
    @IBOutlet weak var userProfileName: UILabel!
    
    @IBOutlet weak var userTimeLine: UITableView!
    @IBOutlet weak var userPost: UITableViewCell!
    @IBOutlet weak var loadingAiv: UIActivityIndicatorView!
    
    var ref = Database.database().reference()
    var loggedInUser:AnyObject?
    var loggedInUserData:NSDictionary?
    var journalEntries = [NSDictionary]()
    
    func tableView(_ userTimeLine: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.journalEntries.count
    }
    
    func tableView(_ userTimeLine: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell: JournalEntryCellTableViewCell = userTimeLine.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! JournalEntryCellTableViewCell
        cell.journalEntryDate.text = journalEntries[(self.journalEntries.count-1) - (indexPath.row)]["journalEntryDate"] as! String
        
           cell.journalEntryText.text = journalEntries[(self.journalEntries.count-1) - (indexPath.row)]["journalEntryText"] as! String
        
        return cell
    }
    
    
    func tableView(_ userTimeLine: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.loggedInUser = Auth.auth().currentUser
        getAllJournalEntries()
        self.userTimeLine.rowHeight = UITableViewAutomaticDimension
        self.userTimeLine.estimatedRowHeight = 140
        self.userProfileName.text = "Sofi"
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func getAllJournalEntries(){
        self.ref.child("users").child(self.loggedInUser!.uid).child("posts").observe(.childAdded, with: { (snapshot:DataSnapshot) in
            
                
                self.journalEntries.append(snapshot.value as! NSDictionary)
                self.userTimeLine.insertRows(at: [IndexPath(row:0,section:0)], with: UITableViewRowAnimation.automatic)
            self.loadingAiv.stopAnimating()
            })
            {(error) in
                
                print(error.localizedDescription)
            }
        self.userTimeLine.rowHeight = UITableViewAutomaticDimension
        self.userTimeLine.estimatedRowHeight = 140
        }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
