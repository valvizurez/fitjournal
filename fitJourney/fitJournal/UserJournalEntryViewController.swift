//
//  UserJournalEntryViewController.swift
//  fitJourney
//
//  Created by Victoria Alvizurez on 2/9/18.
//  Copyright © 2018 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage

class UserJournalEntryViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var userFeelingPicker: UIPickerView!
    @IBOutlet weak var userStartTiem: UITextField!
    @IBOutlet weak var userEndTime: UITextField!
    @IBOutlet weak var todaysDateTitle: UILabel!
    @IBOutlet weak var userJournalEntry: UITextView!
    
    var userFeelingNum = ""
    var curYear = ""
    var curMonth = ""
    var curDay = ""
    
    var userFeelingPickerData = ["1","2","3","4","5","6","7","8","9","10"]
    
    let timePicker = UIDatePicker()
    
     var imagePicker = UIImagePickerController()
    
    func numberOfComponents(in userFeelingPicker: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ userFeelingPicker: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return userFeelingPickerData.count
    }
    func pickerView(_ userFeelingPicker: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return userFeelingPickerData[row]
    }
    func pickerView(_ userFeelingPicker: UIPickerView!, didSelectRow row: Int, inComponent component: Int){
        userFeelingNum = userFeelingPickerData[row] // selected is the variable
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setting todays date
        setCurrentDate()
        showTimePicker()
           }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

  
    //Post journal entry
    @IBAction func userJournalEntryPost(_ sender: Any) {
        
        var imagesArray = [AnyObject]()
        
        //extract the images from the attributed text
        self.userJournalEntry.attributedText.enumerateAttribute(NSAttributedStringKey.attachment, in: NSMakeRange(0, self.userJournalEntry.text.characters.count), options: []) { (value, range, true) in
            
            if(value is NSTextAttachment)
            {
                let attachment = value as! NSTextAttachment
                var image : UIImage? = nil
                
                if(attachment.image !== nil)
                {
                    image = attachment.image!
                    imagesArray.append(image!)
                }
                else
                {
                    print("No image found")
                }
            }
        }
        let journalEntryLength = userJournalEntry.text.characters.count
        let numImages = imagesArray.count
        
        
        let userID = Auth.auth().currentUser!.uid
        let userData = ["journalEntryText": self.userJournalEntry.text, "exStartTime": self.userStartTiem.text, "exEndTime": self.userEndTime.text, "feelingNum": self.userFeelingNum, "journalEntryDate": self.todaysDateTitle.text] as [String : Any]
        
        let ref = Database.database().reference()
        ref.child("users").child(userID).child("posts").childByAutoId().setValue(userData)
        
        let key = ref.child("journalEntries").childByAutoId().key
        
        let storageRef = Storage.storage().reference()
        let pictureStorageRef = storageRef.child("user_profiles/\(userID)/media/\(key)")
        
        if(journalEntryLength>0 && numImages>0)
        {
            let lowResImageData = UIImageJPEGRepresentation(imagesArray[0] as! UIImage, 0.50)
          
            let uploadTask = pictureStorageRef.putData(lowResImageData!,metadata: nil)
            {metadata,error in
                
                if(error == nil)
                {
                    let downloadUrl = metadata!.downloadURL()
                    
                    let childUpdates = ["/journalEntries/\(userID)/\(key)/text":self.userJournalEntry.text,
                                        "/journalEntries/\(userID)/\(key)/timestamp":"\(Date().timeIntervalSince1970)",
                        "/journalEntries/\(userID)/\(key)/picture":downloadUrl!.absoluteString] as [String : Any]
                    
                    ref.updateChildValues(childUpdates)
                }
                
            }
            
            dismiss(animated: true, completion: nil)
        }
        
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "userJournal") as! UserJournalViewController
                self.present(vc, animated: true, completion: nil)
            }
        }
        
    }
    
    //attach image
    @IBAction func userJournalAttachment(_ sender: AnyObject) {
    //        var imagesArray = [AnyObject]()
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum)
        {
           // self.imagePicker.delegate = self.self
            self.imagePicker.sourceType = .savedPhotosAlbum
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
    
        
        var attributedString = NSMutableAttributedString()
        
        if(self.userJournalEntry.text.characters.count>0)
        {
            attributedString = NSMutableAttributedString(string:self.userJournalEntry.text)
        }
        else
        {
            attributedString = NSMutableAttributedString(string:"How was Today?\n")
        }
        
        let textAttachment = NSTextAttachment()
        
        textAttachment.image = image
        
        let oldWidth:CGFloat = textAttachment.image!.size.width
        
        let scaleFactor:CGFloat = oldWidth/(userJournalEntry.frame.size.width-50)
        
        textAttachment.image = UIImage(cgImage: textAttachment.image!.cgImage!, scale: scaleFactor, orientation: .up)
        
        let attrStringWithImage = NSAttributedString(attachment: textAttachment)
        
        attributedString.append(attrStringWithImage)
        
        userJournalEntry.attributedText = attributedString
        self.dismiss(animated: true, completion: nil)
        
        
        
    }

    
    //sets Main title as current date
    func setCurrentDate()
    {
        let currentDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        
        let year = DateFormatter()
        year.dateFormat = "yyyy"
        
        let month = DateFormatter()
        month.dateFormat = "LLLL"
        
        let day = DateFormatter()
        day.dateFormat = "dd"
        
        self.curYear = year.string(from: currentDate)
        self.curMonth = month.string(from: currentDate)
        self.curDay = day.string(from: currentDate)
        todaysDateTitle.text = formatter.string(from: currentDate)
    }
    
    func showTimePicker(){
        //Formate Date
        timePicker.datePickerMode = .time
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.bordered, target: self,  action: #selector(UserJournalEntryViewController.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.bordered, target: self, action:#selector(UserJournalEntryViewController.cancelDatePicker))
        
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        // add toolbar to textField
        userStartTiem.inputAccessoryView = toolbar
        // add datepicker to textField
        userStartTiem.inputView = timePicker
        
        userEndTime.inputAccessoryView = toolbar
        // add datepicker to textField
        userEndTime.inputView = timePicker
    }
    
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        if (userStartTiem.isEditing){
        userStartTiem.text = formatter.string(from: timePicker.date)
        }
        else{
        userEndTime.text = formatter.string(from: timePicker.date)
        }
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
  
}
