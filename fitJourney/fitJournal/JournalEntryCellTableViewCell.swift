//
//  JournalEntryCellTableViewCell.swift
//  fitJourney
//
//  Created by Victoria Alvizurez on 2/18/18.
//  Copyright © 2018 Victoria Alvizurez. All rights reserved.
//

import UIKit

class JournalEntryCellTableViewCell: UITableViewCell {
    @IBOutlet weak var journalEntryDate: UILabel!
    @IBOutlet weak var journalEntryText: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
